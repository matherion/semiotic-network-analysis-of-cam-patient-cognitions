# NETWORK ANALYSIS OF PATIENT DECISION-MAKING REGARDING CHOICE OF THERAPY
We analyzed semi-structured interview data with Epistemic Network Analysis to explore associations among patients' trusted sources of information, their lay etiology, and milestones in their patient journey. We were interested in how the interplay among these factors differ in patients who use biomedicine only and those who employ non-conventional medicine.

The main analysis document is hosted at GitLab pages at https://matherion.gitlab.io/semiotic-network-analysis-of-cam-patient-cognitions.


Directory structure with most important contents (OSF URLs):

Main project URL: https://osf.io/7sm5n/

Research design materials: 
Operationalization with data collection instruments (https://osf.io/839wu)
Complete codebook (https://osf.io/8esnx)
Codes and segmentation (directory: Root --> Codes --> Real) (https://osf.io/726q8)
Complete analysis script (https://osf.io/zbemd)

Supplementary materials for results:
Figure, Intermediate output, and Final output directories in the root directory

Data:
Pretty dataframe (https://osf.io/ejf7b)
(the doc is too large to render, so it needs to be downloaded)
